﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    class SearchedGamesPage : BasePage
    {
        public SearchedGamesPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        // TASK 1.2: implement getSearchWidget function
        public SearchedGamesWidget GetSearchedGamesWidget()
        {
            // Create new searchWidget
            return new SearchedGamesWidget(Driver);
        }

        // TASK 3.2: implement getResultWidget function and instantiate resultWidget
        public ResultWidget GetResultWidget()
        {
            // Create new resultWidget
            return new ResultWidget(Driver);
        }
    }
}
