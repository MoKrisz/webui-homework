﻿using OpenQA.Selenium;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    class SearchPage : BasePage
    {
        public SearchPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static SearchPage navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://www.konzolvilag.hu/";
            return new SearchPage(webDriver);
        }

        public SearchWidget GetSearchWidget()
        {
            return new SearchWidget(Driver);
        }

        public ResultWidget GetResultWidget()
        {
            return new ResultWidget(Driver);
        }
    }
}
