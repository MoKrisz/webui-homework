﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    class CartPage : BasePage
    {
        public CartPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        // TASK 3.2: implement getResultWidget function and instantiate resultWidget
        public CartResultWidget GetCartResultWidget()
        {
            // Create new resultWidget
            return new CartResultWidget(Driver);
        }
    }
}
