﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using WebdriverClass.PagesAtClass;
using WebdriverClass.WidgetsAtClass;
using OpenQA.Selenium;

namespace WebdriverClass
{
    class BeadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("GameData")]
        public void GameDataPageObjectTest(string search, string link, string price)
        {
            try
            {
                SearchWidget searchWidget = SearchPage.navigate(Driver).GetSearchWidget();
                SearchedGamesPage sgp = searchWidget.SearchForGame(search);
                SearchedGamesWidget sgw = sgp.GetSearchedGamesWidget();


                GamePage gp = sgw.ClickOnGame(link);
                GameWidget gw = gp.GetGameWidget();
                ResultWidget rw = gp.GetResultWidget();
                Assert.That(rw.GetGameTitle().Contains(search));


                CartPage cp = gw.PutInCart();
                CartResultWidget crw = cp.GetCartResultWidget();

                Assert.AreEqual(crw.NumberOfGamesInCart(), price);
            }
            catch (Exception)
            {
                var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
                string path = AppDomain.CurrentDomain.BaseDirectory;
                screenshot.SaveAsFile($"{path + search}Test_{DateTime.Now.ToString("yyyy_MM_dd")}_ERROR.png", ScreenshotImageFormat.Png);
            }
        }

        static IEnumerable GameData()
        {
            var datas = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\data.xml");

            var returnable = from games in datas.Descendants("gameData")
                             let search = games.Attribute("search").Value
                             let link = games.Attribute("link").Value
                             let price = games.Attribute("price").Value
                             select new object[] { search, link, price };

            return returnable;
        }

    }
}
