﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using WebdriverClass.PagesAtClass;
using SeleniumExtras.WaitHelpers;

namespace WebdriverClass.WidgetsAtClass
{
    class SearchWidget : BasePage
    {
        public SearchWidget(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search_text")));
            wait.Until(ExpectedConditions.ElementIsVisible(By.Id("search_btn")));

        }

        public IWebElement SearchText => Driver.FindElement(By.Id("search_text"));
        public IWebElement SearchButton => Driver.FindElement(By.Id("search_btn"));

        public SearchedGamesPage SearchForGame(String gameName)
        {
            SearchText.SendKeys(gameName);
            return DirectToGameSearchPage();
        }
        
        public SearchedGamesPage DirectToGameSearchPage()
        {
            SearchButton.Click();
            return new SearchedGamesPage(Driver);
        }
    }
}
