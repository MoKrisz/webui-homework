﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class ResultWidget : BasePage
    {
        public ResultWidget(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("h1[class='title']")));
        }

        public IWebElement GameTitle => Driver.FindElement(By.CssSelector("h1[class='title']"));

        public string GetGameTitle()
        {
            return GameTitle.Text;
        }
    }
}
