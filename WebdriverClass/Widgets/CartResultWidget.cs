﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class CartResultWidget : BasePage
    {
        public CartResultWidget(IWebDriver driver) : base(driver)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            wait.Until(ExpectedConditions.ElementIsVisible(By.XPath("/html/body/main/div/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[3]")));
        }

        private IWebElement GamePrice => Driver.FindElement(By.XPath("/html/body/main/div/div[1]/div[2]/div[1]/table/tbody/tr[1]/td[3]"));

        public string NumberOfGamesInCart()
        {
            return GamePrice.Text;
        }
    }
}
