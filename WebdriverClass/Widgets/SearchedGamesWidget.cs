﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class SearchedGamesWidget : BasePage
    {
        public SearchedGamesWidget(IWebDriver driver) : base(driver)
        {
        }

        public GamePage ClickOnGame(string linkname)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            var element = wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector(linkname)));
            element.Click();
            return new GamePage(Driver);
        }
    }
}
