﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class GameWidget : BasePage
    {
        public GameWidget(IWebDriver driver) : base(driver)
        {
        }

        private IWebElement CartButton => Driver.FindElement(By.Id("addToBasket"));
        private IWebElement HeaderCart => Driver.FindElement(By.CssSelector("a[id='header-cart']"));

        public CartPage PutInCart()
        {
            CartButton.Click();
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(ExpectedConditions.ElementIsVisible(By.CssSelector("div[id*='status-message'][class*='remodal-is-opened']")));
            var element = wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//*[@id='status-message']/button")));
            //Thread.Sleep(2000); //enélkül nem működik, hiába hívom meg az expectedconditionst.
            element.Click();
            wait.Until(ExpectedConditions.ElementToBeClickable(HeaderCart));
            HeaderCart.Click();
            HeaderCart.Click();
            return new CartPage(Driver);
        }
    }
}
